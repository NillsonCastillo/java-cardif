package main;

import entities.Cabecera;
import entities.DTES;
import entities.Detalle;
import enums.AcronymDteEnum;
import enums.DteEnum;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class main {

    public static void main(String[] args) throws IOException {
        long ti = System.currentTimeMillis();
        createFileDte(DTES.FACTURA_ANTICIPADA);
        long to = System.currentTimeMillis();
        //System.out.println(((to - ti) / 1000) * 60 + " Segundos");
    }

    public static String createDirectoryPathDte(Integer dteType) throws IOException {
        String dir = System.getProperty("user.dir");
        File directory = new File(dir + DTES.PATH_DTES);
        String directoryPath = directory.getAbsolutePath();

        if (!directory.exists() || directory.isDirectory()) {
            directory.mkdirs();
        }

        switch (dteType) {
            case DTES.NOTA_CREDITO:
                File notaCreditoFolder = new File(directoryPath + DTES.NOTA_CREDITO_PATH);
                notaCreditoFolder.mkdirs();
                return notaCreditoFolder.getPath();
            case DTES.NOTA_DEBITO:
                File notaDebitoFolder = new File(directoryPath + DTES.NOTA_DEBITO_PATH);
                notaDebitoFolder.mkdirs();
                return notaDebitoFolder.getPath();
            case DTES.FACTURA:
            case DTES.FACTURA_ANTICIPADA:
                File facturaFolder = new File(directoryPath + DTES.NOTA_FACTURA_PATH);
                facturaFolder.mkdirs();
                return facturaFolder.getPath();
            case DTES.BOLETA:
                File boletaFolder = new File(directoryPath + DTES.BOLETA_PATH);
                boletaFolder.mkdirs();
                return boletaFolder.getPath();
            default:
                return "No se pudo crear el directorio para el DTE" + dteType;
        }
    }

    public static void createFileDte(int dteId, int... request) throws IOException {
        Date now = new Date();
        SimpleDateFormat date = new SimpleDateFormat("ddMMyyyyHHmmss");
        List<Detalle> array = new ArrayList<Detalle>();
        Cabecera cabecera = new Cabecera();
        String path = createDirectoryPathDte(dteId);

        ResultSet resultSetCabecera = null;
        ResultSet resultSetDetalle = null;

        /*String queryCabecera = "Query cabecera";
        String queryDetalle = "Query detalle";

        resultSetCabecera = DBAConnection.executeQuery(queryCabecera);
        resultSetDetalle = DBAConnection.executeQuery(queryDetalle);
        */

        for (int i = 0; i < 4; i++) {
            array.add(new Detalle(Integer.toString(i),
                    Integer.toString(i),
                    Integer.toString(i),
                    Integer.toString(i)
            ));
        }

        switch (dteId) {
            case DTES.NOTA_CREDITO:
                String nc = setDteName(AcronymDteEnum.NOTAS_CREDITO, 93, DteEnum.NOTAS_CREDITO);
                File dteNc = new File(path + nc + DTES.EXTENSION_TXT);
                double sizeNcFile = getFileMegaBytes(dteNc);
                if (!dteNc.exists() || dteNc.isFile() || (sizeNcFile < DTES.MAX_FILE_SIZE))
                    dteNc.createNewFile();
                writeDteData(cabecera, array, dteNc.getAbsolutePath());
                break;
            case DTES.NOTA_DEBITO:
                String nd = setDteName(AcronymDteEnum.NOTAS_DEBITO, 93, DteEnum.NOTAS_DEBITO);
                File dteNd = new File(path + nd + DTES.EXTENSION_TXT);
                double sizeNdFile = getFileMegaBytes(dteNd);
                if (!dteNd.exists() || dteNd.isFile() || (sizeNdFile < DTES.MAX_FILE_SIZE))
                    dteNd.createNewFile();
                writeDteData(cabecera, array, dteNd.getAbsolutePath());
                break;
            case DTES.FACTURA:
                String factura = setDteName(AcronymDteEnum.FACTURA, 93, DteEnum.FACTURA);
                File dteFac = new File(path + factura + DTES.EXTENSION_TXT);
                double sizeFacturaFile = getFileMegaBytes(dteFac);
                if (!dteFac.exists() || dteFac.isFile() || (sizeFacturaFile < DTES.MAX_FILE_SIZE))
                    dteFac.createNewFile();
                writeDteData(cabecera, array, dteFac.getAbsolutePath());
                break;
            case DTES.FACTURA_ANTICIPADA:
                String facturaAnticipada = setDteName(AcronymDteEnum.FACTURA_ANTICIPADA, 96, DteEnum.FACTURA_ANTICIPADA);
                File dteFacAnt = new File(path + facturaAnticipada + DTES.EXTENSION_TXT);
                double sizeFacturaAntiFile = getFileMegaBytes(dteFacAnt);
                if (!dteFacAnt.exists() || dteFacAnt.isFile() || (sizeFacturaAntiFile < DTES.MAX_FILE_SIZE))
                    dteFacAnt.createNewFile();
                writeDteData(cabecera, array, dteFacAnt.getAbsolutePath());
                break;
            case DTES.BOLETA:
                String boleta = setDteName(AcronymDteEnum.BOLETA, 93, DteEnum.BOLETA);
                File dteBol = new File(path + boleta + DTES.EXTENSION_TXT);
                double sizeBolFile = getFileMegaBytes(dteBol);
                if (!dteBol.exists() || dteBol.isFile() || (sizeBolFile < DTES.MAX_FILE_SIZE))
                    dteBol.createNewFile();
                writeDteData(cabecera, array, dteBol.getAbsolutePath());
                break;
            default:
                System.out.println("No se reconoce el codigo del DTE. No se puede crear documentos");
                break;
        }
    }

    public static void writeDteData(Cabecera cabecera, List<Detalle> detalle, String path) throws IOException {
        try (PrintWriter pr = new PrintWriter(path, "UTF-8")) {
            String totalesHeaders = DTES.TOTALES_DTE;
            Double montoUnitario = 0.0;
            pr.println(cabecera);

            for (Detalle d : detalle) {
                montoUnitario = montoUnitario + Double.parseDouble(d.montoUnitarioPesos);
                pr.println(d);
            }
            pr.println(totalesHeaders
                    .replace("Monto_venta", String.valueOf(montoUnitario))
                    .replace("Venta_Neta_Afecta_Pesos", String.valueOf(montoUnitario))
                    .replace("Venta_Neta_Exenta_Pesos", "0.0")
                    .replace("Monto_Iva_Pesos", "0.0")
                    .replace("Total_Bruto", String.valueOf(montoUnitario))
            );
            loadPrebilling(cabecera, detalle, totalesHeaders);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setTimeOut(Runnable runnable, int delay) {
        new Thread(() -> {
            try {
                Thread.sleep(delay);
                runnable.run();
            } catch (Exception e) {
                System.err.println(e);
            }
        });
    }

    public static String setDteName(AcronymDteEnum acronymDteEnum, int agrupId, DteEnum dteEnum) {
        Date now = new Date();
        SimpleDateFormat date = new SimpleDateFormat("ddMMyyyyHHmmss");
        StringBuffer dtePath = new StringBuffer("/Archivo_Prefacturacion");
        dtePath.append("_").append(date.format(now));
        dtePath.append("_").append(agrupId);
        dtePath.append("_").append("Compañia");
        dtePath.append("_").append(acronymDteEnum.getAcronym());
        dtePath.append("_").append(dteEnum.getIdDte());
        return dtePath.toString();
    }

    public static double getFileMegaBytes(File file) {
        return (double) file.length() / (1024 * 1024);
    }

    public static <T> String convertObjectToString(T object) {
        return object.toString();
    }

    public static String regExpForSpecialCharacters() {
        return "\\W";
    }

    public static <T> String clearInfoDte(T object) {
        StringBuffer auxParameterString = new StringBuffer(convertObjectToString(object)).delete(0, 3);
        auxParameterString.deleteCharAt(auxParameterString.length() - 1);
        return convertObjectToString(auxParameterString).replaceAll(regExpForSpecialCharacters(), ",");
    }

    public static void loadPrebilling(Cabecera cabecera, List<Detalle> detalle, String totales) {
        String auxCabeceraString = clearInfoDte(cabecera);
        String auxTotalesString = clearInfoDte(totales);
        StringBuffer queryInserteDteInfo = new StringBuffer("INSERT INTO car_archprefactura (");

        List<String> arrayDetalles = new ArrayList<String>();
        List<String> arrayCabecera = new ArrayList<>(Arrays.asList(auxCabeceraString.split(",")));
        List<String> arrayTotales = new ArrayList<>(Arrays.asList(auxTotalesString.split(",")));

        for (Detalle detail : detalle) {
            arrayDetalles.add(clearInfoDte(detail));
        }

        //System.out.println(auxCabeceraString);
        for (int i = 0; i < arrayDetalles.size(); i++) {
            System.out.println(arrayDetalles.get(i));
        }
        //System.out.println(auxTotalesString);
        //queryInserteDteInfo.append()
        queryInserteDteInfo.append(" VALUES ('").append("HOLA");
        queryInserteDteInfo.append("', '").append("Bruce");
        queryInserteDteInfo.append("')");
        System.out.println(queryInserteDteInfo.toString());

    }

}



/*System.out.println("Generar objetos de tipo Genericos");

        Plano<Integer> planoInt = new Plano<Integer>(0, 0, 0, 0);
        Plano<Double> planoDouble = new Plano<Double>(1.3, 1.6, 1.4, 30.8);
        Plano<String> planoString = new Plano<>("Hola", "como", "Estan", "Todos");

        System.out.println(planoInt);
        System.out.println(planoDouble);
        System.out.println(planoString);*/