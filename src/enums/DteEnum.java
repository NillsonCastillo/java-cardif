package enums;

public enum DteEnum {
    NOTAS_CREDITO(61),
    NOTAS_DEBITO(62),
    FACTURA(63),
    FACTURA_ANTICIPADA(64),
    BOLETA(65);

    public int idDte;

    DteEnum(int idDte){
        this.idDte = idDte;
    }

    public int getIdDte() {
        return idDte;
    }

}
