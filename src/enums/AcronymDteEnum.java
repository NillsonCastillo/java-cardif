package enums;

public enum AcronymDteEnum {
    NOTAS_CREDITO("NC"),
    NOTAS_DEBITO("ND"),
    FACTURA("CF"),
    FACTURA_ANTICIPADA("FA"),
    BOLETA("BO");

    String acronym;

    AcronymDteEnum(String acronym) {
        this.acronym = acronym;
    }

    public String getAcronym() {
        return acronym;
    }
}
