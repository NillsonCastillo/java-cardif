package genericos;

public class Plano<T>  {

    public T x1,x2,x3,x4;

    public Plano(T x1, T x2, T x3, T x4){
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
    }

    public T getX1() {
        return x1;
    }

    public void setX1(T x1) {
        this.x1 = x1;
    }

    public T getX2() {
        return x2;
    }

    public void setX2(T x2) {
        this.x2 = x2;
    }

    public T getX3() {
        return x3;
    }

    public void setX3(T x3) {
        this.x3 = x3;
    }

    public T getX4() {
        return x4;
    }

    public void setX4(T x4) {
        this.x4 = x4;
    }

    @Override
    public String toString() {
        return "Plano{" +
                "x1=" + x1 +
                ", x2=" + x2 +
                ", x3=" + x3 +
                ", x4=" + x4 +
                '}';
    }
}
