package genericos.clases;

public class Vehiculo {

    public int acelerar(int x1, int x2, int t1, int t2){
        int aceleracion = (x2 - x1)/(t2 - t1);
        return aceleracion;
    }
}
