package entities;

public class DTES {
    public static final int NOTA_CREDITO = 61;
    public static final int NOTA_DEBITO = 62;
    public static final int FACTURA = 63;
    public static final int FACTURA_ANTICIPADA = 64;
    public static final int BOLETA = 65;
    public static final double MAX_FILE_SIZE = 9.9;
    public static final String FACTURA_ANTICIPADA_STRING = "Anticipada";
    public static final String PATH_DTES = "/assets/dtes";
    public static final String NOTA_CREDITO_PATH = "/NotasDeCredito/";
    public static final String NOTA_DEBITO_PATH = "/NotasDeDebito/";
    public static final String NOTA_FACTURA_PATH = "/Facturas/";
    public static final String BOLETA_PATH = "/Boletas/";
    public static final String EXTENSION_TXT = ".txt";
    public static final String TOTALES_DTE = "03<Monto_venta;Venta_Neta_Afecta_Pesos;Venta_Neta_Exenta_Pesos;"
                                            + "Monto_Iva_Pesos;Total_Bruto>";


}
