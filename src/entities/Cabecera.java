package entities;

public class Cabecera {
    public String compañia;
    public String sistema = "Cartola";
    public String areaResponsable = "";
    public String jefeAreaResponsable = "";
    public String emitidoPor;
    public String fechaEmision;
    public String fechaUf;
    public String valorUf;
    public String mesContable;
    public String rutCliente;
    public String nombreCliente = "";
    public String direccionCliente;
    public String comunaCliente;
    public String ciudadCliente;
    public String codigoGiroPims;
    public String desGiroPims;
    public String tipoEnvioPims;
    public String direccionEnvio;
    public String tipoDocumento;
    public String numeroDocumentoInterno = "";
    public String tipoFactura;
    public String tipoVenta;
    public String ventaDirectaIntermediada;
    public String fechaVencimiento;
    public String tipoDocumentoReferencia;
    public String folioDocumentoReferencia;
    public String fechaReferencia;
    public String codigoReferencia;
    public String razonReferencia;

    @Override
    public String toString() {
        return "01<" + compañia + ";" + sistema + ";" + areaResponsable + ";" + jefeAreaResponsable + ";"
                + emitidoPor + ";" + fechaUf + ";" + valorUf + ";" + mesContable + ";" + rutCliente + ";"
                + nombreCliente + ";" + direccionCliente + ";" + comunaCliente + ";" + ciudadCliente + ";"
                + codigoGiroPims + ";" + desGiroPims + ";" + tipoEnvioPims + ";" + direccionEnvio + ";"
                + tipoDocumento + ";" + numeroDocumentoInterno + ";" + tipoFactura + ";" + tipoVenta + ";"
                + ventaDirectaIntermediada + ";" + fechaVencimiento + ";" + tipoDocumentoReferencia + ";"
                + folioDocumentoReferencia + ";" + fechaReferencia + ";" + codigoReferencia + ";" + razonReferencia
                + ">";
    }

    public Cabecera() {
    }

    public Cabecera(String compañia, String sistema ,String emitidoPor, String fechaEmision, String fechaUf, String valorUf,
                    String mesContable, String rutCliente, String direccionCliente, String comunaCliente,
                    String ciudadCliente, String codigoGiroPims, String desGiroPims, String tipoEnvioPims,
                    String direccionEnvio, String tipoDocumento, String tipoFactura, String tipoVenta,
                    String ventaDirectaIntermediada, String fechaVencimiento, String tipoDocumentoReferencia,
                    String folioDocumentoReferencia, String fechaReferencia, String codigoReferencia,
                    String razonReferencia) {
        super();
        this.compañia = compañia;
        this.sistema = sistema;
        this.areaResponsable = "";
        this.jefeAreaResponsable = "";
        this.emitidoPor = emitidoPor;
        this.fechaEmision = fechaEmision;
        this.fechaUf = fechaUf;
        this.valorUf = valorUf;
        this.mesContable = mesContable;
        this.rutCliente = rutCliente;
        this.nombreCliente = "";
        this.direccionCliente = direccionCliente;
        this.comunaCliente = comunaCliente;
        this.ciudadCliente = ciudadCliente;
        this.codigoGiroPims = codigoGiroPims;
        this.desGiroPims = desGiroPims;
        this.tipoEnvioPims = tipoEnvioPims;
        this.direccionEnvio = direccionEnvio;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumentoInterno = "";
        this.tipoFactura = tipoFactura;
        this.tipoVenta = tipoVenta;
        this.ventaDirectaIntermediada = ventaDirectaIntermediada;
        this.fechaVencimiento = fechaVencimiento;
        this.tipoDocumentoReferencia = tipoDocumentoReferencia;
        this.folioDocumentoReferencia = folioDocumentoReferencia;
        this.fechaReferencia = fechaReferencia;
        this.codigoReferencia = codigoReferencia;
        this.razonReferencia = razonReferencia;
    }
}
