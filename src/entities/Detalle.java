package entities;

public class Detalle {
    public String poliza = "123456";
    public String cantidadPolizas = "";
    public String detalle = "";
    public String montoUnitarioUfProductos;
    public String montoUnitarioPesos;
    public String condicionesGenerales = "";
    public String mesCobertura;
    public String ramoFecu = "";
    public String codigoAfecto;

    @Override
    public String toString() {
        return "02<" + montoUnitarioUfProductos + ";" + montoUnitarioPesos + ";"
                + mesCobertura + ";" + codigoAfecto + ">";
    }

    public Detalle(String montoUnitarioUfProductos, String montoUnitarioPesos, String mesCobertura, String codigoAfecto) {
        super();
        this.montoUnitarioUfProductos = montoUnitarioUfProductos;
        this.montoUnitarioPesos = montoUnitarioPesos;
        this.mesCobertura = mesCobertura;
        this.codigoAfecto = codigoAfecto;
    }
}
